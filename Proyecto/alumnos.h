#define MAX 180
#ifndef alumnos_h
#define alumnos_h
struct alumnos
{
    int edad;
    char nombre[120];
    char genero;
    char carrera[50];
    char nCuenta[10];
    float promedio;
};
typedef struct alumnos ALUMNO;

int indiceAlArreglo=0;
ALUMNO listaAlumnos[MAX];


void insertarAlumno(ALUMNO al){
    if (indiceAlArreglo >= 0 && indiceAlArreglo < MAX) {
        listaAlumnos[indiceAlArreglo]=al;
        indiceAlArreglo++;
    }else{
        printf("El indice apunta fuera del arreglo, favor de revisar la lógica");
    }
}


ALUMNO nuevoAlumno(){
    ALUMNO tmp;
    printf("Introduce la edad:");
    scanf("%d",&tmp.edad);
    printf("Introduce el genero [M o F]:");
    scanf(" %c",&tmp.genero);
    printf("Introduce el nombre:");
    scanf("%*c%[^\n]",tmp.nombre);
    printf("Introduce Carrera:");
    scanf("%*c%[^\n]",tmp.carrera);
    printf("Introduce numero de cuenta:");
    scanf("%*c%[^\n]",tmp.nCuenta);
    printf("Introduce tu promedio:");
    scanf("%f",&tmp.promedio);

    insertarAlumno(tmp);
    return tmp;
}

void imprimeAlumno(ALUMNO alu){
    printf("\tNombre:%s\n",alu.nombre);
    printf("\tEdad:%d\n",alu.edad);
    printf("\tGenero:%c\n",alu.genero);
    printf("\tCarrera:%s\n",alu.carrera);
    printf("\tNumero de Cuenta:%s\n",alu.nCuenta);
    printf("\tPromedio:%.2f\n",alu.promedio);
    printf("+---------------------------------+\n\n");
}


void imprimirLista(){
    int j=0;
    for (j = 0; j < indiceAlArreglo; j++) {
        printf("+--------- # de lista: %d ---------+*\n",j+1);
        imprimeAlumno(listaAlumnos[j]);
    }
}

int menu(){
    int op=0;
    printf("\n----------- Menú para la aplicacion de BD para alumnos ---------\n");
    printf("(1) Crear lista.\n");
    printf("(2) Guardar a archivo.\n");
    printf("(3) Leer desde archivo.\n");
    printf("(4) Mostrar lista.\n");
    printf("(5) Agregar alumno. \n");
    printf("(6) Obtener promedio de alumnos.\n");
    printf("(7) Buscar alumno por nombre. \n");
    printf("(8) Buscar alumno por edad. \n");
    printf("(9) Eliminar alumno. \n");
    printf("(10) Modificar datos \n");
    printf("(11) Buscar por carrera. \n");
    printf("(12) Buscar po genero \n");
    printf("(0) SALIR\n");
    printf("\n\nElige una opcion:");
    scanf("%d",&op);

    return op;
}

void grabaRegistros(ALUMNO r[], int tam){
    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","w"))==NULL){
        printf("el archivo no se puede abrir\n");
    }else{
        fwrite(r,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

void leerRegistros(int tam){

    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        fread(listaAlumnos,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

int registrosEnArchivo(){
    FILE *ptrF;
    int contador=0;
    ALUMNO  basura;
    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        while(!feof(ptrF)){
            if (fread(&basura,sizeof(ALUMNO),1,ptrF))
                contador++;
        }

    }
    fclose(ptrF);
    return contador;
}
#endif

void buscarNombre() {
  int i =0;
char nombre[20];
printf("Introduzca el nombre del alumno:" );
scanf("%s",nombre);
for ( i = 0; i <= indiceAlArreglo; i++) {
if (strcmp(nombre,listaAlumnos[i].nombre)==0) {
  imprimeAlumno(listaAlumnos[i]);
  }
 }
}

void buscarEdad() {
  int i = 0;
  int edad = 0;
printf("Introduce la edad del alumno:" );
scanf("%d",&edad);
for ( i = 0; i <= indiceAlArreglo; i++) {
if (edad== listaAlumnos[i].edad) {
  imprimeAlumno(listaAlumnos[i]);
  }
 }
}

void eliminarAlumno(){
int i =0,numero=0;
printf("Dame el numero de lista:" );
scanf("%d",&numero);
for ( i = numero-1; i <= indiceAlArreglo; i++) {
  listaAlumnos[i]=listaAlumnos[i+1];
 }
 indiceAlArreglo=indiceAlArreglo-1;
}


void imprimirPromedio(){
  int j = 0;
  float temporalpromedio=0.0,promediog;
  for (j = 0; j < indiceAlArreglo; j++) {
    temporalpromedio=temporalpromedio+listaAlumnos[j].promedio;
  }
  promediog=temporalpromedio/indiceAlArreglo;
    printf("El promedio del grupo es %.2f \n", promediog);
  }

void Modificar(){
  int i =0,numero=0;
  int op=0;
  printf("Dame el numero de lista del alumnno:" );
  scanf("%d",&numero);
  for ( i = numero-1; i <= indiceAlArreglo; i++) {
    listaAlumnos[i]=listaAlumnos[i];
   }
   imprimeAlumno(listaAlumnos[numero-1]);

    printf("\n----------- Elige el campo ---------\n");
    printf("(1) Edad.\n");
    printf("(2) Nombre.\n");
    printf("(3) Carrera.\n");
    printf("(4) Numero de cuenta.\n");
    printf("(5) Promedio.\n");
    printf("\nElige una opcion:");
    scanf("%d",&op);
    switch (op) {
      case 1:
      printf("La edad es:%d\n",listaAlumnos[numero-1].edad );
      printf("Introduce la nueva edad:");
      scanf("%d",&listaAlumnos[numero-1].edad);
      imprimeAlumno(listaAlumnos[numero-1]);
      break;
      case 2:
      printf("El nombre es:%s\n",listaAlumnos[numero-1].nombre);
      printf("Introduce el nombre correcto:");
      scanf("%s",listaAlumnos[numero-1].nombre);
      imprimeAlumno(listaAlumnos[numero-1]);
      break;
      case 3:
      printf("La carrera es:%s\n",listaAlumnos[numero-1].carrera);
      printf("Introduce la carrera correcta:");
      scanf("%s",listaAlumnos[numero-1].carrera);
      imprimeAlumno(listaAlumnos[numero-1]);
      break;
      case 4:
      printf("El Num. de cuenta es:%s\n",listaAlumnos[numero-1].nCuenta);
      printf("Introduce el nombre correcto:");
      scanf("%s",listaAlumnos[numero-1].nCuenta);
      imprimeAlumno(listaAlumnos[numero-1]);
      break;
      case 5:
      printf("El promedio  es:%.2f\n",listaAlumnos[numero-1].promedio);
      printf("Introduce el promedio correcto:");
      scanf("%f",&listaAlumnos[numero-1].promedio);
      imprimeAlumno(listaAlumnos[numero-1]);
      break;
      }
}
