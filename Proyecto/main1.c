#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "alumnos.h"

int main(int argc, const char * argv[]) {
    int opcion=0;
    int cantidad=0;
    int j=0; 
    printf("Programa de alumnos!\n");
    do {
        opcion=menu();

        switch (opcion) {
            case 1:
                 printf("Crear una nueva lista de alumnos\n");
                 printf("¿Cuantos alumnos quieres crear?:");
                 scanf("%d",&cantidad);
                 for(j=0;j<cantidad;j++){
                   printf("Capturando datos del alumno %d\n",j+1);
                   nuevoAlumno();
                 }
                break;

            case 2:
                printf("Guardar lista al archivo\n");
                grabaRegistros(listaAlumnos,indiceAlArreglo);
                break;

            case 3:
                printf("Leer la lista desde el archivo\n");
                indiceAlArreglo=registrosEnArchivo();
                leerRegistros(indiceAlArreglo);
                break;

            case 4:
                printf("Mostrar todos los datos de la lista\n");
                imprimirLista();
                break;

            case 5:
                printf("Agregar un nuevo alumno a la lista\n");
                nuevoAlumno();
                break;

            case 6:
                printf("Promedio general del grupo\n");
                imprimirPromedio();
                break;

            case 7:
                printf("Buscar por nombre");
                buscarNombre();
                break;

            case 8:
                printf("Buscar por edad");
                buscarEdad();
                break;

            case 9:
                printf("Eliminar Datos");
                eliminarAlumno();
                printf("Eliminando datos...");
                break;

            case 10:
               printf("Modificar datos");
               Modificar();
                break;

              default:
              printf("Opcion no valida");
              break;

        }

    } while (opcion != 0);
    return 0;
}
